import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';

abstract class DB {
  Future<DocumentSnapshot> checkUserRole(String userID);
  Stream<QuerySnapshot> fetchCourts();
  Future<DocumentSnapshot> addData(
      String eventTitle, eventTime, eventDate, eventDescription);
  Future<DocumentSnapshot> removeData(String key);
  Future<DocumentSnapshot> updateData(String id, Map<String, dynamic> data);
  Future<DocumentSnapshot> updateSched(selectedDoc, newValues);
}

class Database implements DB {
  Future<DocumentSnapshot> checkUserRole(String userID) async {
    return await FirebaseFirestore.instance
        .collection("users")
        .doc(userID)
        .get();
  }

  Stream<QuerySnapshot> fetchCourts() {
    return FirebaseFirestore.instance.collection("courts").snapshots();
  }

  Future<DocumentSnapshot> addData(
      String eventTitle, eventTime, eventDate, eventDescription) {
    return FirebaseFirestore.instance.collection("events").doc().set({
      "eventTitle": eventTitle,
      "eventTime": eventTime,
      "eventDate": eventDate,
      "eventDescription": eventDescription
    });
  }

  Future<DocumentSnapshot> updateData(String id, Map<String, dynamic> data) {
    return FirebaseFirestore.instance.collection("events").doc(id).update(data);
  }

  Future<DocumentSnapshot> updateSched(selectedDoc, newValues) {
    return FirebaseFirestore.instance
        .collection('schedules')
        .doc(selectedDoc)
        .update(newValues)
        .catchError((e) {
      print(e);
    });
  }

  Future<DocumentSnapshot> removeData(String key) {
    return FirebaseFirestore.instance.collection("events").doc(key).delete();
  }
}

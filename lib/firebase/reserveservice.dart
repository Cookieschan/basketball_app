import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:basketball_app/model/reservefunction.dart';

final CollectionReference myCollection =
    FirebaseFirestore.instance.collection('reserve');

class ReserveService {
  Future<Reserve> createReserve(
      String reserveName,
      String reserveNumber,
      String reserveTimeStart,
      String reserveTimeEnd,
      String reserveDate,
      String reserveCourtName) async {
    final TransactionHandler createTransaction = (Transaction tx) async {
      final DocumentSnapshot ds = await tx.get(myCollection.doc());

      final Reserve reserve = Reserve(reserveName, reserveNumber,
          reserveTimeStart, reserveTimeEnd, reserveDate, reserveCourtName);
      final Map<String, dynamic> data = reserve.toMap();
      tx.set(ds.reference, data);
      return data;
    };

    return FirebaseFirestore.instance
        .runTransaction(createTransaction)
        .then((mapData) {
      return Reserve.fromMap(mapData);
    }).catchError((error) {
      print('error: $error');
      return null;
    });
  }

  Stream<QuerySnapshot> getReserveList({int offset, int limit}) {
    Stream<QuerySnapshot> snapshots = myCollection.snapshots();

    if (offset != null) {
      snapshots = snapshots.skip(offset);
    }
    if (limit != null) {
      snapshots = snapshots.take(limit);
    }
    return snapshots;
  }
}

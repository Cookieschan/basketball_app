import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:basketball_app/model/eventfunction.dart';

final CollectionReference myCollection =
    FirebaseFirestore.instance.collection('events');

class FirestoreService {
  Future<Sched> createEvent(String eventTitle, String eventTime,
      String eventDate, String eventDescription) async {
    final TransactionHandler createTransaction = (Transaction tx) async {
      final DocumentSnapshot ds = await tx.get(myCollection.doc());

      final Sched sched =
          new Sched(eventTitle, eventTime, eventDate, eventDescription);
      final Map<String, dynamic> data = sched.toMap();
      tx.set(ds.reference, data);
      return data;
    };

    return FirebaseFirestore.instance
        .runTransaction(createTransaction)
        .then((mapData) {
      return Sched.fromMap(mapData);
    }).catchError((error) {
      print('error: $error');
      return null;
    });
  }

  Stream<QuerySnapshot> getSchedList({int offset, int limit}) {
    Stream<QuerySnapshot> snapshots = myCollection.snapshots();

    if (offset != null) {
      snapshots = snapshots.skip(offset);
    }
    if (limit != null) {
      snapshots = snapshots.take(limit);
    }
    return snapshots;
  }
}

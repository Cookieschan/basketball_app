import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';

final CollectionReference courtCollection =
    FirebaseFirestore.instance.collection('Court');

final CollectionReference eventCollection =
    FirebaseFirestore.instance.collection('Court').doc().collection('events');

class EventService {
  Stream<QuerySnapshot> getEventList({int offset, int limit}) {
    Stream<QuerySnapshot> snapshots = eventCollection.snapshots();

    if (offset != null) {
      snapshots = snapshots.skip(offset);
    }
    if (limit != null) {
      snapshots = snapshots.take(limit);
    }
    return snapshots;
  }

  Stream<QuerySnapshot> getCourtList({int offset, int limit}) {
    Stream<QuerySnapshot> snapshots = courtCollection.snapshots();

    if (offset != null) {
      snapshots = snapshots.skip(offset);
    }
    if (limit != null) {
      snapshots = snapshots.take(limit);
    }
    return snapshots;
  }
}

import 'package:flutter/material.dart';

class FormTitle extends StatelessWidget {
  final String text;
  
  const FormTitle({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          text,
          style: TextStyle(
              fontSize: 25, color: Colors.white, fontWeight: FontWeight.normal),
        ),
      ),
    );
  }
}

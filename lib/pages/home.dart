import 'package:basketball_app/firebase/auth.dart';
import 'package:basketball_app/firebase/database.dart';
import 'package:basketball_app/main.dart';
import 'package:basketball_app/pages/dashboard/event.dart';
import 'package:basketball_app/pages/dashboard/map.dart';
import 'package:basketball_app/pages/dashboard/notification.dart';
import 'package:basketball_app/pages/dashboard/schedule.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'League Management/admin.dart';

class Home extends StatefulWidget {
  Home({Key key, this.auth, this.database}) : super(key: key);

  final BaseAuth auth;
  final Database database;

  @override
  State<StatefulWidget> createState() => new _HomeState();
}

class _HomeState extends State<Home> {
  User _user;
  int _selectedPage = 0;
  List<Widget> _widgetOptions;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _widgetOptions = <Widget>[
      MapPage(),
      SchedulePage(),
      EventPage(),
      NotificationPage(),
    ];
    widget.auth.getCurrentUser().then((user) {
      print(user.uid);
      setState(() {
        _user = user;
      });
    });

    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage message) {
      if (message != null) {
        setState(() {
          _selectedPage = 3;
        });
      }
    });

    // AndroidInitializationSettings initializationSettingsAndroid =
    //     AndroidInitializationSettings('app_notf_icon');

    // InitializationSettings initializationSettings =
    //     InitializationSettings(android: initializationSettingsAndroid);

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,

                channel.description,
                // TODO add a proper drawable resource to android, for now using
                //      one that already exists in example app.
                icon: 'launch_background',
                playSound: true,
              ),
            ));
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('A new onMessageOpenedApp event was published!');
      print(message.data);
      setState(() {
        _selectedPage = 3;
      });
    });
  }

  void _onItemTapped(int index) {
    print("Tapped");
    setState(() {
      _selectedPage = index;
    });
  }

  Icon cusIcon = Icon(Icons.search);
  Widget cusSearchBar = Text("Basketball App");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<DocumentSnapshot>(
          future: widget.database.checkUserRole(_user.uid), // Check use role
          builder:
              (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.exists &&
                  snapshot.data.data()['role'] == "admin") {
                return Scaffold(
                    appBar: AppBar(title: Text(''), actions: <Widget>[
                      new IconButton(
                        icon: new Icon(Icons.exit_to_app),
                        onPressed: () => widget.auth.signOut(),
                      ),
                    ]),
                    drawer: Drawer(
                      child: ListView(
                        children: <Widget>[
                          DrawerHeader(
                              decoration: BoxDecoration(
                                gradient: LinearGradient(colors: <Color>[]),
                              ),
                              child: Text('Hi Admin')),
                          ListTile(
                            title: Text('View Events'),
                            onTap: () {
                              Navigator.pushNamed(context, '/eventformview');
                            },
                          ),
                          ListTile(
                            title: Text('View Schedules'),
                            onTap: () {
                              Navigator.pushNamed(context, '/schedformview');
                            },
                          ),
                          ListTile(
                            leading: Icon(Icons.settings),
                            title: Text("Settings"),
                            onTap: () =>
                                Navigator.pushNamed(context, '/settings'),
                          ),
                          ListTile(
                            title: Text(''),
                          ),
                        ],
                      ),
                    ),
                    body: IconTheme.merge(
                        data: IconThemeData(
                          color: Theme.of(context).primaryColorDark,
                        ),
                        child: Column(children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 1.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: Icon(Icons.person, size: 80.0),
                                    ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text('Hello'),
                                          Text(
                                            'Admin',
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20.0,
                                            ),
                                          ),
                                          Text(''),
                                        ],
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Text(''),
                                        Text(''),
                                        Text(''),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Divider(height: 10),
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: DashboardButton(
                                    icon: Icons.book,
                                    text: 'RESERVE',
                                    onTap: () {
                                      Navigator.pushNamed(
                                          context, '/reserveformview');
                                    },
                                  ),
                                ),
                                Divider(height: 10),
                                Expanded(
                                  child: DashboardButton(
                                    icon: Icons.schedule,
                                    text: 'CREATE SCHEDULE',
                                    onTap: () {
                                      Navigator.pushNamed(
                                          context, '/scheduleform');
                                    },
                                  ),
                                ),
                                Divider(height: 10),
                                Expanded(
                                  child: DashboardButton(
                                    icon: Icons.edit_location,
                                    text: 'CREATE EVENT',
                                    onTap: () {
                                      Navigator.pushNamed(
                                          context, '/eventform');
                                    },
                                  ),
                                ),
                                Divider(height: 10),
                                Expanded(
                                  child: DashboardButton(
                                    icon: Icons.location_on,
                                    text: 'VIEW MAP',
                                    onTap: () {
                                      Navigator.pushNamed(context, '/mapadmin');
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ])));
              } else {
                return Scaffold(
                    appBar: AppBar(
                      actions: <Widget>[
                        IconButton(
                          icon: cusIcon,
                          onPressed: () {
                            setState(() {
                              if (this.cusIcon.icon == Icons.search) {
                                this.cusIcon = Icon(Icons.cancel);
                                this.cusSearchBar = TextField(
                                  textInputAction: TextInputAction.go,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Courts",
                                  ),
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                  ),
                                );
                              } else {
                                this.cusIcon = Icon(Icons.search);
                                this.cusSearchBar = Text("Bassketball App");
                              }
                            });
                          },
                        )
                      ],
                      title: cusSearchBar,
                    ),
                    drawer: new Drawer(
                      child: new ListView(
                        children: <Widget>[
                          new UserAccountsDrawerHeader(
                            accountName: new Text("${_user?.displayName}"),
                            accountEmail: new Text("${_user?.email}"),
                            currentAccountPicture: new CircleAvatar(
                              backgroundColor: Colors.white,
                              backgroundImage:
                                  NetworkImage("${_user?.photoURL}"),
                            ),
                          ),
                          new ListTile(
                            leading: Icon(Icons.assessment),
                            title: Text("Games & Scores"),
                          ),
                          new ListTile(
                            leading: Icon(Icons.star),
                            title: Text("Leagues"),
                          ),
                          new ListTile(
                            leading: Icon(Icons.payment),
                            title: Text("News"),
                          ),
                          new ListTile(
                            leading: Icon(Icons.history),
                            title: Text("Schedule history"),
                          ),
                          new Divider(),
                          new ListTile(
                            leading: Icon(Icons.settings),
                            title: Text("Settings"),
                            onTap: () =>
                                Navigator.pushNamed(context, '/settings'),
                          ),
                          new Divider(),
                          new ListTile(
                            title: Text("Sign Out"),
                            trailing: new Icon(Icons.exit_to_app),
                            onTap: () => widget.auth.signOut(),
                          ),
                        ],
                      ),
                    ),
                    body: Center(
                      child: _widgetOptions.elementAt(_selectedPage),
                    ),
                    bottomNavigationBar: BottomNavigationBar(
                      items: const <BottomNavigationBarItem>[
                        BottomNavigationBarItem(
                            icon: Icon(Icons.home), title: Text('Home')),
                        BottomNavigationBarItem(
                            icon: Icon(Icons.query_builder),
                            title: Text('Schedule')),
                        BottomNavigationBarItem(
                            icon: Icon(Icons.event), title: Text('Events')),
                        BottomNavigationBarItem(
                            icon: Icon(Icons.notifications),
                            title: Text('Notification')),
                      ],
                      currentIndex: _selectedPage,
                      selectedItemColor: Colors.blueAccent,
                      unselectedItemColor: Colors.grey,
                      onTap: _onItemTapped,
                    ));
              }
            } else {
              Text("No role data");
            }
            return Center(child: CircularProgressIndicator());
          }),
    );
  }
}

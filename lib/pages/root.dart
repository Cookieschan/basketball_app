import 'package:basketball_app/firebase/auth.dart';
import 'package:basketball_app/firebase/database.dart';
import 'package:basketball_app/pages/home.dart';
import 'package:basketball_app/pages/welcome.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class RootPage extends StatefulWidget {
  RootPage({this.auth, this.database});

  final BaseAuth auth;
  final Database database;

  @override
  State<StatefulWidget> createState() => new _RootPageState();
}

class _RootPageState extends State<RootPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<User>(
      stream: widget.auth.checkAuthStatus(),
      builder: (BuildContext context, snapshot) {
        if (snapshot.hasData) {
          return new Home(
            auth: widget.auth,
            database: widget.database,
          );
        } else {
          return new WelcomePage();
        }
      },
    );
  }
}

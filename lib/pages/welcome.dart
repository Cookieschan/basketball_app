import 'package:basketball_app/widgets/brand.dart';
import 'package:basketball_app/widgets/form_title.dart';
import 'package:flutter/material.dart';

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        backgroundColor: Colors.orangeAccent,
        body: Container(
          padding:
              EdgeInsets.only(top: 0.0, right: 20.0, left: 20.0, bottom: 0.0),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Brand(),
                SizedBox(height: 20.0),
                FormTitle(text: "Welcome!"), // Custom widget
                Container(
                  padding: EdgeInsets.only(
                      top: 20.0,
                      right: 0,
                      left: 0,
                      bottom: 0.0), //size sang button
                  child: MaterialButton(
                    minWidth: double.infinity,
                    child: Text("Login"),
                    color: Colors.amber,
                    onPressed: () {
                      Navigator.pushNamed(context, '/login');
                    },
                  ),
                ),
                SizedBox(height: 20.0),
                Container(
                  padding:
                      EdgeInsets.only(top: 0.0, right: 0, left: 0, bottom: 0.0),
                  child: MaterialButton(
                    minWidth: double.infinity,
                    child: Text("Register"),
                    color: Colors.blue,
                    onPressed: () {
                      Navigator.pushNamed(context, '/signup');
                    },
                  ),
                ),
                FlatButton(
                  textColor: Colors.white,
                  onPressed: () {
                    Navigator.pushNamed(context, '/termscon');
                  },
                  child: Text(
                    "Terms & Conditions",
                    textAlign: TextAlign.right,
                  ),
                )
              ]),
        ));
  }
}

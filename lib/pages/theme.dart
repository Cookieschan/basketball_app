 
import 'package:flutter/material.dart';

final lightTheme = ThemeData(
  primarySwatch: Colors.amber,
  primaryColor: Colors.amber,
  brightness: Brightness.light,
  backgroundColor: const Color(0xFFE5E5E5),
  accentColor: Colors.blueAccent,
  accentIconTheme: IconThemeData(color: Colors.amber),
  dividerColor: Colors.blue,
  
  
  
);

final darkTheme = ThemeData(
  primarySwatch: Colors.grey,
  primaryColor: Colors.black,
  brightness: Brightness.dark,
  backgroundColor: const Color(0xFF212121),
  accentColor: Colors.grey,
  accentIconTheme: IconThemeData(color: Colors.black),
  dividerColor: Colors.black,
  
);
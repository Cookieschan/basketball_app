import 'package:basketball_app/firebase/auth.dart';
import 'package:basketball_app/widgets/brand.dart';
import 'package:basketball_app/widgets/form_title.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  LoginPage({this.auth});

  final BaseAuth auth;

  @override
  State<StatefulWidget> createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  String _userEmail;
  String _userPassword;
  bool _isLoading;

  set _errorMessage(_errorMessage) {}

  void initState() {
    _isLoading = false;
    super.initState();
  }

  bool isFormValidated() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    setState(() {
      _isLoading = false;
    });
    return false;
  }

  void signIn() async {
    print("Signing in...");
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (isFormValidated()) {
      String user = "";

      try {
        User user = await widget.auth.signIn(_userEmail, _userPassword);
        if (user != null && await user.getIdToken() != null) {
          final User currentUser = await widget.auth.getCurrentUser();
          if (currentUser.uid == user.uid) {
            print("Success in Signing In");
            Navigator.of(context).popUntil((route) => route.isFirst);
          }
        }
      } catch (e) {
        print(e.message);
        return showDialog<void>(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Email or Password is incorrect'),
              content: const Text(
                  'Please retry inputing your Email/Password or Check your Internet Connection'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.pushNamed(context, '/login');
                  },
                ),
              ],
            );
          },
        );
      }
    }
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.deepPurpleAccent,
        body: Stack(
          children: <Widget>[
            _showCircularProgress(),
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Brand(),
                    Container(
                        padding:
                            EdgeInsets.only(top: 30.0, left: 20.0, right: 20.0),
                        child: Form(
                          key: _formKey,
                          child: Column(children: <Widget>[
                            FormTitle(text: "Login"),
                            SizedBox(height: 40.0), // Custom widget
                            TextFormField(
                              style: new TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                  labelText: 'Email Address',
                                  labelStyle: TextStyle(
                                      fontWeight: FontWeight.normal,
                                      color: Colors.white),
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white))),
                              validator: (value) => value.isEmpty
                                  ? 'User Email can\'t be empty'
                                  : null,
                              onSaved: (value) => _userEmail = value.trim(),
                            ),
                            SizedBox(height: 20.0),
                            TextFormField(
                              style: new TextStyle(color: Colors.white),
                              decoration: InputDecoration(
                                  labelText: 'Password',
                                  labelStyle: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.normal,
                                      color: Colors.white),
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white))),
                              obscureText: true,
                              validator: (value) => value.isEmpty
                                  ? 'User Password can\'t be empty'
                                  : null,
                              onSaved: (value) => _userPassword = value.trim(),
                            ),
                            SizedBox(height: 40.0),
                            Container(
                              padding: EdgeInsets.only(
                                  top: 0.0, right: 0, left: 0, bottom: 0.0),
                              color: Colors.transparent,
                              child: MaterialButton(
                                minWidth: double.infinity,
                                child: Text("Login"),
                                color: Colors.amber,
                                onPressed: () {
                                  this.signIn();
                                },
                              ),
                            ),
                            SizedBox(height: 20.0),
                            Container(
                              padding: EdgeInsets.only(
                                  top: 0.0, right: 0, left: 0, bottom: 0.0),
                              color: Colors.transparent,
                              child: MaterialButton(
                                minWidth: double.infinity,
                                child: Text("Cancel"),
                                color: Colors.blueAccent,
                                onPressed: () {
                                  Navigator.pushNamed(context, '/welcome');
                                },
                              ),
                            ),
                            SizedBox(
                              width: 5.0,
                            ),
                            Text(
                              'or',
                              style: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5.0,
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top: 0.0, right: 0, left: 0, bottom: 0.0),
                              color: Colors.transparent,
                              child: MaterialButton(
                                minWidth: double.infinity,
                                child: Text("Login with Google"),
                                color: Colors.green,
                                onPressed: () async {
                                  bool res =
                                      await widget.auth.loginWithGoogle();
                                  Navigator.of(context)
                                      .popUntil((route) => route.isFirst);
                                  if (!res)
                                    print("error loggin in with google");
                                },
                              ),
                            ),
                          ]),
                        )),
                  ]),
            )
          ],
        ));
  }
}

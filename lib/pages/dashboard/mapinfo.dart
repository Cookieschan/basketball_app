import 'package:auto_size_text/auto_size_text.dart';
import 'package:basketball_app/model/basketball_model.dart';
import 'package:basketball_app/pages/dashboard/mapreservationform.dart';
import 'package:flutter/material.dart';

class MapInfoView extends StatefulWidget {
  final Court court;
  MapInfoView(this.court);

  @override
  _MapInfoViewState createState() => _MapInfoViewState();
}

class _MapInfoViewState extends State<MapInfoView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: AutoSizeText(
            'Reserve a Court',
            style: TextStyle(fontSize: 17.0),
            minFontSize: 10,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Stack(children: <Widget>[
              // The containers in the background
              SingleChildScrollView(
                  child: Column(children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 800.0,
                        child: Padding(
                            padding: EdgeInsets.only(top: 8.0, bottom: 2.0),
                            child: Material(
                                color: Colors.white,
                                elevation: 2.0,
                                shadowColor: Color(0x802196F3),
                                child: Center(
                                  child: Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            // Court Image
                                            Card(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          bottomLeft: Radius
                                                              .circular(10.0),
                                                          bottomRight:
                                                              Radius.circular(
                                                                  10.0),
                                                          topRight:
                                                              Radius.circular(
                                                                  10.0),
                                                          topLeft:
                                                              Radius.circular(
                                                                  10.0))),
                                              elevation: 10.0,
                                              child: Container(
                                                  height: 200.0,
                                                  width: 360.0,
                                                  decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.only(
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  10.0),
                                                          bottomRight:
                                                              Radius.circular(
                                                                  10.0),
                                                          topRight:
                                                              Radius.circular(
                                                                  10.0),
                                                          topLeft:
                                                              Radius.circular(
                                                                  10.0)),
                                                      image: DecorationImage(
                                                          image: NetworkImage(
                                                              '${widget.court.thumbNail}'),
                                                          fit: BoxFit.cover))),
                                            ),
                                            SizedBox(
                                              height: 20.0,
                                            ),
                                            // Court Name
                                            Container(
                                                child: AutoSizeText(
                                              '${widget.court.courtName}',
                                              style: TextStyle(
                                                fontSize: 25.0,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.deepOrangeAccent,
                                              ),
                                              minFontSize: 10,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                            )),
                                            SizedBox(
                                              height: 50.0,
                                            ),
                                            // Court Address
                                            AutoSizeText(
                                              'address',
                                              style: TextStyle(
                                                fontSize: 20.0,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.deepOrangeAccent,
                                              ),
                                              minFontSize: 10,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            Container(
                                                child: AutoSizeText(
                                              '${widget.court.address}',
                                              style: TextStyle(fontSize: 20.0),
                                              minFontSize: 10,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                            )),
                                            SizedBox(
                                              height: 50.0,
                                            ),
                                            // Court Operating Hours
                                            AutoSizeText(
                                              'operating hours',
                                              style: TextStyle(
                                                fontSize: 20.0,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.deepOrangeAccent,
                                              ),
                                              minFontSize: 10,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            Container(
                                                child: AutoSizeText(
                                              '${widget.court.optHours}',
                                              style: TextStyle(fontSize: 20.0),
                                              minFontSize: 10,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                            )),
                                            SizedBox(
                                              height: 50.0,
                                            ),
                                            // Court Surface
                                            AutoSizeText(
                                              'surfaces',
                                              style: TextStyle(
                                                fontSize: 20.0,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.deepOrangeAccent,
                                              ),
                                              minFontSize: 10,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            Container(
                                                child: AutoSizeText(
                                              '${widget.court.surfaces}',
                                              style: TextStyle(fontSize: 20.0),
                                              minFontSize: 10,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                            )),
                                            SizedBox(
                                              height: 50.0,
                                            ),
                                            // Court Rates and Inclusions
                                            AutoSizeText(
                                              'rates and inclusions',
                                              style: TextStyle(
                                                fontSize: 20.0,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.deepOrangeAccent,
                                              ),
                                              minFontSize: 10,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            Container(
                                                child: AutoSizeText(
                                              '${widget.court.rateIncl}',
                                              style: TextStyle(fontSize: 20.0),
                                              minFontSize: 10,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                            )),
                                            SizedBox(
                                              height: 20.0,
                                            ),
                                            Container(
                                                padding: EdgeInsets.only(
                                                    top: 0.0,
                                                    right: 0.0,
                                                    left: 0.0,
                                                    bottom: 0.0),
                                                height: 40.0,
                                                color: Colors.transparent,
                                                child: Container(
                                                    padding: EdgeInsets.only(
                                                        top: 0.0,
                                                        right: 10.0,
                                                        left: 10.0,
                                                        bottom: 0.0),
                                                    color: Colors.transparent,
                                                    child: Center(
                                                        child: RaisedButton(
                                                            color: Theme.of(
                                                                    context)
                                                                .accentColor,
                                                            child: const Text(
                                                                'Reserve'),
                                                            onPressed: () {
                                                              Navigator.push(
                                                                  context,
                                                                  new MaterialPageRoute(
                                                                      builder:
                                                                          (context) =>
                                                                              MapreservationFormPage()));
                                                            }))))
                                          ])),
                                )))))
              ]))
            ])));
  }
}

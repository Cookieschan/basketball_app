import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:table_calendar/table_calendar.dart';

class MapreservationFormPage extends StatefulWidget {
  MapreservationFormPage();
  @override
  State<StatefulWidget> createState() => new _MapreservationFormPageState();
}

class _MapreservationFormPageState extends State<MapreservationFormPage> {
  CalendarController _controller;
  Map<DateTime, List<dynamic>> _events;
  List<dynamic> _selectedEvents;
  TextEditingController _eventController;
  SharedPreferences prefs;
  String eventTitle, eventTime, eventDate, eventDescription;

  geteventTitle(eventTitle) {
    this.eventTitle = eventTitle;
  }

  geteventTime(eventTime) {
    this.eventTime = eventTime;
  }

  geteventDate(eventDate) {
    this.eventDate = eventDate;
  }

  geteventDescription(eventDescription) {
    this.eventDescription = eventDescription;
  }

  @override
  void initState() {
    super.initState();
    _controller = CalendarController();
    _eventController = TextEditingController();
    _events = {};
    _selectedEvents = [];
    initPrefs();
  }

  initPrefs() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      _events = Map<DateTime, List<dynamic>>.from(
          decodeMap(json.decode(prefs.getString('events') ?? "{}")));
    });
  }

  Map<String, dynamic> encodeMap(Map<DateTime, dynamic> map) {
    Map<String, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[key.toString()] = map[key];
    });
    return newMap;
  }

  Map<DateTime, dynamic> decodeMap(Map<String, dynamic> map) {
    Map<DateTime, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[DateTime.parse(key)] = map[key];
    });
    return newMap;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Calendar'),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TableCalendar(
              events: _events,
              initialCalendarFormat: CalendarFormat.week,
              calendarStyle: CalendarStyle(
                  canEventMarkersOverflow: true,
                  todayColor: Colors.orange,
                  selectedColor: Theme.of(context).primaryColor,
                  todayStyle: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                      color: Colors.white)),
              headerStyle: HeaderStyle(
                centerHeaderTitle: true,
                formatButtonDecoration: BoxDecoration(
                  color: Colors.orange,
                  borderRadius: BorderRadius.circular(20.0),
                ),
                formatButtonTextStyle: TextStyle(color: Colors.white),
                formatButtonShowsNext: false,
              ),
              startingDayOfWeek: StartingDayOfWeek.monday,
              onDaySelected: (date, events, _) {
                setState(() {
                  _selectedEvents = events;
                });
              },
              builders: CalendarBuilders(
                selectedDayBuilder: (context, date, events) => Container(
                    margin: const EdgeInsets.all(4.0),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(10.0)),
                    child: Text(
                      date.day.toString(),
                      style: TextStyle(color: Colors.white),
                    )),
                todayDayBuilder: (context, date, events) => Container(
                    margin: const EdgeInsets.all(4.0),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Colors.orange,
                        borderRadius: BorderRadius.circular(10.0)),
                    child: Text(
                      date.day.toString(),
                      style: TextStyle(color: Colors.white),
                    )),
              ),
              calendarController: _controller,
            ),
            ..._selectedEvents.map((event) => ListTile(
                  title: Text(event),
                )),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: _showAddDialog,
      ),
    );
  }

  _showAddDialog() {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
            backgroundColor: Colors.deepPurpleAccent,
            content: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.only(
                              top: 10.0, left: 20.0, right: 20.0),
                          child: Form(
                              child: Column(
                            children: <Widget>[
                              TextFormField(
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                    labelText: 'Set Title',
                                    labelStyle: TextStyle(
                                        fontFamily: 'Montserrat',
                                        color: Colors.white),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.white))),
                                validator: (value) => value.isEmpty
                                    ? 'Title can\'t be empty'
                                    : null,
                                onChanged: (String title) {
                                  geteventTitle(title);
                                },
                              ),
                              TextFormField(
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                    labelText: 'Set Time',
                                    labelStyle: TextStyle(
                                        fontFamily: 'Montserrat',
                                        color: Colors.white),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.white))),
                                validator: (value) => value.isEmpty
                                    ? 'Title can\'t be empty'
                                    : null,
                                onChanged: (String time) {
                                  geteventTime(time);
                                },
                              ),
                              TextFormField(
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                    labelText: 'Set Date',
                                    labelStyle: TextStyle(
                                        fontFamily: 'Montserrat',
                                        color: Colors.white),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.white))),
                                validator: (value) => value.isEmpty
                                    ? 'Title can\'t be empty'
                                    : null,
                                onChanged: (String date) {
                                  geteventDate(date);
                                },
                              ),
                              TextFormField(
                                style: TextStyle(color: Colors.white),
                                decoration: InputDecoration(
                                    labelText: 'Set Description',
                                    labelStyle: TextStyle(
                                        fontFamily: 'Montserrat',
                                        color: Colors.white),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.white))),
                                validator: (value) => value.isEmpty
                                    ? 'Title can\'t be empty'
                                    : null,
                                onChanged: (String description) {
                                  geteventDescription(description);
                                },
                              ),
                              FlatButton(
                                child: Text("Save"),
                                onPressed: () {
                                  if (_eventController.text.isEmpty) return;
                                  setState(() {
                                    if (_events[_controller.selectedDay] !=
                                        null) {
                                      _events[_controller.selectedDay]
                                          .add(_eventController.text);
                                    } else {
                                      _events[_controller.selectedDay] = [
                                        _eventController.text
                                      ];
                                    }
                                    prefs.setString("events",
                                        json.encode(encodeMap(_events)));
                                    _eventController.clear();
                                    Navigator.pop(context);
                                  });
                                },
                              )
                            ],
                          )))
                    ]))));
  }
}

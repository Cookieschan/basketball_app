import 'dart:async';

import 'package:basketball_app/firebase/database.dart';
import 'package:basketball_app/firebase/schedservice.dart';
import 'package:basketball_app/model/schedulefunction.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class SchedulePage extends StatefulWidget {
  SchedulePage({Key key, this.database}) : super(key: key);
  Database database;
  @override
  _SchedulePageState createState() => _SchedulePageState();
}

class _SchedulePageState extends State<SchedulePage> {
  List<Court> courts;
  EventService fireServ = new EventService();
  StreamSubscription<QuerySnapshot> listCourts;

  @override
  void initState() {
    super.initState();

    courts = [];
    listCourts?.cancel();
    listCourts = fireServ.getCourtList().listen((QuerySnapshot snapshot) {
      final List<Court> court = snapshot.docs
          .map((documentSnapshot) => Court.fromMap(
              new Map.from(documentSnapshot.data())
                ..addAll({'key': documentSnapshot.id})))
          .toList();

      setState(() {
        this.courts = court;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: courts.length,
        itemBuilder: (context, index) {
          return Column(
            children: <Widget>[
              ListTile(
                  title: Text('${courts[index].courtName}'),
                  subtitle: Text('${courts[index].address}'),
                  onTap: () {}),
              Divider(),
            ],
          );
        });
  }
}

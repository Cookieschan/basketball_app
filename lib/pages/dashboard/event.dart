import 'dart:async';

import 'package:basketball_app/firebase/firestoreservice.dart';
import 'package:basketball_app/model/eventfunction.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class EventPage extends StatefulWidget {
  EventPage({Key key}) : super(key: key);

  @override
  _EventPageState createState() => _EventPageState();
}

class _EventPageState extends State<EventPage> {
  List<Sched> items;
  FirestoreService fireServ = new FirestoreService();
  StreamSubscription<QuerySnapshot> toEvents;

  @override
  void initState() {
    super.initState();

    items = [];

    toEvents?.cancel();
    toEvents = fireServ.getSchedList().listen((QuerySnapshot snapshot) {
      final List<Sched> events = snapshot.docs
          .map((documentSnapshot) => Sched.fromMap(documentSnapshot.data()))
          .toList();

      setState(() {
        this.items = events;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
            width: MediaQuery.of(context).size.width,
            height: 615.0,
            child: ListView.builder(
                itemCount: items.length,
                itemBuilder: (context, index) {
                  return Stack(children: <Widget>[
                    // The containers in the background
                    Column(
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(left: 8.0, right: 8.0),
                            child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: 341.0,
                                child: Padding(
                                    padding:
                                        EdgeInsets.only(top: 8.0, bottom: 2.0),
                                    child: Material(
                                        elevation: 2.0,
                                        shadowColor: Color(0xFFEEEEEE),
                                        child: Center(
                                            child: Padding(
                                                padding: EdgeInsets.all(8.0),
                                                child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Flexible(
                                                        child: Text(
                                                          '${items[index].eventTitle}',
                                                          style: TextStyle(
                                                              fontSize: 20.0,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 5.0,
                                                      ),
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Text(
                                                            '${items[index].eventTime}',
                                                            style: TextStyle(
                                                                fontSize: 10.0),
                                                          ),
                                                          SizedBox(
                                                            width: 5.0,
                                                          ),
                                                          Text(
                                                            '${items[index].eventDate}',
                                                            style: TextStyle(
                                                                fontSize: 10.0),
                                                          ),
                                                        ],
                                                      ),
                                                      SizedBox(
                                                        height: 5.0,
                                                      ),
                                                      Text(
                                                        '${items[index].eventDescription}',
                                                        style: TextStyle(
                                                            fontSize: 18.0),
                                                      ),
                                                      SizedBox(height: 12),
                                                    ])))))))
                      ],
                    ),
                  ]);
                })));
  }
}

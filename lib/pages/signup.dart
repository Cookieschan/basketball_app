import 'package:basketball_app/firebase/auth.dart';
import 'package:basketball_app/widgets/brand.dart';
import 'package:basketball_app/widgets/form_title.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SignUpPage extends StatefulWidget {
  SignUpPage({this.auth});

  final BaseAuth auth;

  @override
  State<StatefulWidget> createState() => new _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final _formKey = GlobalKey<FormState>();

  String _userEmail;
  String _userPassword;

  bool isFormValidated() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void signUp() async {
    if (isFormValidated()) {
      String user = "";
      try {
        widget.auth.signUp(_userEmail, _userPassword).then((user) {
          if (_formKey.currentState.validate()) {}
          print('User ID $user');
          return showDialog<void>(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Good Job!'),
                content: const Text('You can now login'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      widget.auth.signOut();
                      Navigator.pushNamed(context, '/welcome');
                    },
                  ),
                ],
              );
            },
          );
        });
      } catch (e) {
        print('Error: $e');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.deepPurpleAccent,
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: <
                  Widget>[
            Brand(),
            Container(
                padding: EdgeInsets.only(top: 50.0, left: 20.0, right: 20.0),
                child: Form(
                    key: _formKey,
                    child: Column(children: <Widget>[
                      FormTitle(text: "Register"), // Custom widget
                      TextFormField(
                        decoration: InputDecoration(
                            labelText: 'Email Address',
                            labelStyle: TextStyle(
                                fontFamily: 'Montserrat', color: Colors.white),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white))),
                        validator: (value) =>
                            value.isEmpty ? 'User Email can\'t be empty' : null,
                        onSaved: (value) => _userEmail = value.trim(),
                      ),
                      SizedBox(height: 20.0),
                      TextFormField(
                        decoration: InputDecoration(
                            labelText: 'Password',
                            labelStyle: TextStyle(
                                fontFamily: 'Montserrat', color: Colors.white),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white))),
                        obscureText: true,
                        validator: (value) =>
                            value.isEmpty ? 'Password can\'t be empty' : null,
                        onSaved: (value) => _userPassword = value.trim(),
                      ),
                      SizedBox(height: 40.0),
                      Container(
                        padding: EdgeInsets.only(
                            top: 0.0, right: 0.0, left: 0.0, bottom: 0.0),
                        height: 40.0,
                        color: Colors.transparent,
                        child: Container(
                          padding: EdgeInsets.only(
                              top: 0.0, right: 10.0, left: 10.0, bottom: 0.0),
                          color: Colors.transparent,
                          child: MaterialButton(
                            minWidth: double.infinity,
                            child: Text("Register"),
                            color: Colors.amber,
                            onPressed: () {
                              this.signUp();
                            },
                          ),
                        ),
                      ),
                      SizedBox(height: 20.0),
                      Container(
                        padding: EdgeInsets.only(
                            top: 0.0, right: 0.0, left: 0.0, bottom: 0.0),
                        height: 40.0,
                        color: Colors.transparent,
                        child: Container(
                          padding: EdgeInsets.only(
                              top: 0.0, right: 10.0, left: 10.0, bottom: 0.0),
                          color: Colors.transparent,
                          child: MaterialButton(
                            minWidth: double.infinity,
                            child: Text("Cancel"),
                            color: Colors.white,
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ),
                      SizedBox(height: 20.0),
                    ])))
          ]),
        ));
  }
}

import 'package:flutter/material.dart';
import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:basketball_app/firebase/firestoreservice.dart';
import 'package:basketball_app/model/eventfunction.dart';
import 'package:flutter/rendering.dart';

class EventFormView extends StatefulWidget {
  EventFormView({Key key}) : super(key: key);

  @override
  _EventFormViewState createState() => _EventFormViewState();
}

class _EventFormViewState extends State<EventFormView> {
  List<Sched> items;
  FirestoreService fireServ = new FirestoreService();
  StreamSubscription<QuerySnapshot> toEvents;
  final ds = FirebaseFirestore.instance;
  String eventTitle, eventTime, eventDate, eventDescription;

  geteventTitle(eventTitle) {
    this.eventTitle = eventTitle;
  }

  geteventTime(eventTime) {
    this.eventTime = eventTime;
  }

  geteventDate(eventDate) {
    this.eventDate = eventDate;
  }

  geteventDescription(eventDescription) {
    this.eventDescription = eventDescription;
  }

  @override
  void initState() {
    super.initState();

    items = [];

    toEvents?.cancel();
    toEvents = fireServ.getSchedList().listen((QuerySnapshot snapshot) {
      final List<Sched> events = snapshot.docs
          .map((documentSnapshot) => Sched.fromMap(documentSnapshot.data()))
          .toList();

      setState(() {
        this.items = events;
      });
    });
  }

  Card buildItem(DocumentSnapshot doc) {
    return Card(
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                    Widget>[
              Text(
                '${doc.data()['eventTitle']}',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
              Text(
                '${doc.data()['eventTime']}',
                style: TextStyle(fontSize: 20),
              ),
              Text(
                '${doc.data()['eventDate']}',
                style: TextStyle(fontSize: 20),
              ),
              Text(
                '${doc.data()['eventDescription']}',
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(height: 12),
              Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
                FlatButton(
                  onPressed: () {
                    showDialog<void>(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                              backgroundColor: Colors.deepPurpleAccent,
                              content: SingleChildScrollView(
                                  scrollDirection: Axis.vertical,
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                            padding: EdgeInsets.only(
                                                top: 10.0,
                                                left: 20.0,
                                                right: 20.0),
                                            child: Form(
                                                child: Column(
                                              children: <Widget>[
                                                TextFormField(
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                  decoration: InputDecoration(
                                                      labelText: 'Set Title',
                                                      labelStyle: TextStyle(
                                                          fontFamily:
                                                              'Montserrat',
                                                          color: Colors.white),
                                                      focusedBorder:
                                                          UnderlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Colors
                                                                      .white))),
                                                  validator: (value) => value
                                                          .isEmpty
                                                      ? 'Title can\'t be empty'
                                                      : null,
                                                  onChanged: (String title) {
                                                    geteventTitle(title);
                                                  },
                                                ),
                                                TextFormField(
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                  decoration: InputDecoration(
                                                      labelText: 'Set Time',
                                                      labelStyle: TextStyle(
                                                          fontFamily:
                                                              'Montserrat',
                                                          color: Colors.white),
                                                      focusedBorder:
                                                          UnderlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Colors
                                                                      .white))),
                                                  validator: (value) => value
                                                          .isEmpty
                                                      ? 'Title can\'t be empty'
                                                      : null,
                                                  onChanged: (String time) {
                                                    geteventTime(time);
                                                  },
                                                ),
                                                TextFormField(
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                  decoration: InputDecoration(
                                                      labelText: 'Set Date',
                                                      labelStyle: TextStyle(
                                                          fontFamily:
                                                              'Montserrat',
                                                          color: Colors.white),
                                                      focusedBorder:
                                                          UnderlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Colors
                                                                      .white))),
                                                  validator: (value) => value
                                                          .isEmpty
                                                      ? 'Title can\'t be empty'
                                                      : null,
                                                  onChanged: (String date) {
                                                    geteventDate(date);
                                                  },
                                                ),
                                                TextFormField(
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                  decoration: InputDecoration(
                                                      labelText:
                                                          'Set Description',
                                                      labelStyle: TextStyle(
                                                          fontFamily:
                                                              'Montserrat',
                                                          color: Colors.white),
                                                      focusedBorder:
                                                          UnderlineInputBorder(
                                                              borderSide: BorderSide(
                                                                  color: Colors
                                                                      .white))),
                                                  validator: (value) => value
                                                          .isEmpty
                                                      ? 'Title can\'t be empty'
                                                      : null,
                                                  onChanged:
                                                      (String description) {
                                                    geteventDescription(
                                                        description);
                                                  },
                                                ),
                                                FlatButton(
                                                  child: Text('Ok'),
                                                  onPressed: () {
                                                    updateData(
                                                        doc.id,
                                                        this.eventTitle,
                                                        this.eventTime,
                                                        this.eventDate,
                                                        this.eventDescription);
                                                    Navigator.pop(context);
                                                  },
                                                ),
                                              ],
                                            )))
                                      ])));
                        });
                  },
                  child: Text('Update event',
                      style: TextStyle(color: Colors.white)),
                  color: Colors.green,
                ),
                SizedBox(width: 8),
                FlatButton(
                  onPressed: () => deleteData(doc),
                  child: Text('Delete', style: TextStyle(color: Colors.white)),
                  color: Colors.red,
                ),
              ])
            ])));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Events'),
      ),
      body: ListView(
        padding: EdgeInsets.all(8),
        children: <Widget>[
          StreamBuilder<QuerySnapshot>(
            stream: ds.collection('events').snapshots(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Column(
                    children: snapshot.data.docs
                        .map((documentSnapshot) => buildItem(documentSnapshot))
                        .toList());
              } else {
                return SizedBox();
              }
            },
          )
        ],
      ),
    );
  }

  void updateData(String documentID, String eventTitle, String eventTime,
      String eventDate, String eventDescription) async {
    print("Updating data...");
    return await ds.collection('events').doc(documentID).update({
      "eventTitle": this.eventTitle,
      "eventTime": this.eventTime,
      "eventDate": this.eventDate,
      "eventDescription": this.eventDescription
    });
  }

  void deleteData(DocumentSnapshot doc) async {
    try {
      await ds.collection('events').doc(doc.id).delete();

      setState(() => this.items == null);
    } catch (e) {}
  }
}

import 'package:basketball_app/widgets/form_title.dart';
import 'package:flutter/material.dart';
import 'package:basketball_app/widgets/brand.dart';
import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class SchedFormPage extends StatefulWidget {
  SchedFormPage();
  @override
  State<StatefulWidget> createState() => new _SchedFormPageState();
}

class _SchedFormPageState extends State<SchedFormPage> {
  final _formKey = GlobalKey<FormState>();
  String teamOne, teamTwo, teamTime, teamDate, teamAddress;

  getteamOne(teamOne) {
    this.teamOne = teamOne;
  }

  getteamTwo(teamTwo) {
    this.teamTwo = teamTwo;
  }

  getteamTime(teamTime) {
    this.teamTime = teamTime;
  }

  getteamDate(teamDate) {
    this.teamDate = teamDate;
  }

  getteamAddress(teamAddress) {
    this.teamAddress = teamAddress;
  }

  createData() {
    DocumentReference ds =
        FirebaseFirestore.instance.collection('schedules').doc();
    Map<String, dynamic> sched = {
      "teamOne": teamOne,
      "teamTwo": teamTwo,
      "teamTime": teamTime,
      "teamDate": teamDate,
      "teamAddress": teamAddress,
    };
    if (_formKey.currentState.validate())
      ds.set(sched).whenComplete(() {
        print("New Schedule is added");
      });
    if (_formKey.currentState.validate()) {
      return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Success'),
            content: const Text('new schedule is added'),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.blueAccent,
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Brand(),
                Container(
                  padding: EdgeInsets.only(top: 10.0, left: 20.0, right: 20.0),
                  child: Form(
                    key: _formKey,
                    child: Column(children: <Widget>[
                      FormTitle(text: "Create Schedules"),
                      TextFormField(
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            labelText: 'Enter Team One',
                            labelStyle: TextStyle(
                                fontFamily: 'Montserrat', color: Colors.white),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white))),
                        validator: (value) =>
                            value.isEmpty ? 'This field can\'t be empty' : null,
                        onChanged: (String one) {
                          getteamOne(one);
                        },
                      ),
                      SizedBox(height: 5.0),
                      TextFormField(
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            labelText: 'Enter Team Two',
                            labelStyle: TextStyle(
                                fontFamily: 'Montserrat', color: Colors.white),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white))),
                        validator: (value) =>
                            value.isEmpty ? 'This field can\'t be empty' : null,
                        onChanged: (String two) {
                          getteamTwo(two);
                        },
                      ),
                      SizedBox(height: 5.0),
                      TextFormField(
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            labelText: 'Enter Time',
                            labelStyle: TextStyle(
                                fontFamily: 'Montserrat', color: Colors.white),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white))),
                        validator: (value) =>
                            value.isEmpty ? 'This field can\'t be empty' : null,
                        onChanged: (String time) {
                          getteamTime(time);
                        },
                      ),
                      SizedBox(height: 5.0),
                      TextFormField(
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            labelText: 'Enter Date',
                            labelStyle: TextStyle(
                                fontFamily: 'Montserrat', color: Colors.white),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white))),
                        validator: (value) =>
                            value.isEmpty ? 'This field can\'t be empty' : null,
                        onChanged: (String date) {
                          getteamDate(date);
                        },
                      ),
                      SizedBox(height: 5.0),
                      TextFormField(
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            labelText: 'Enter Address',
                            labelStyle: TextStyle(
                                fontFamily: 'Montserrat', color: Colors.white),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white))),
                        validator: (value) =>
                            value.isEmpty ? 'This field can\'t be empty' : null,
                        onChanged: (String address) {
                          getteamAddress(address);
                        },
                      ),
                    ]),
                  ),
                ),
                SizedBox(height: 40.0),
                Container(
                  padding: EdgeInsets.only(
                      top: 0.0, right: 0.0, left: 0.0, bottom: 0.0),
                  height: 40.0,
                  color: Colors.transparent,
                  child: Container(
                    padding: EdgeInsets.only(
                        top: 0.0, right: 10.0, left: 10.0, bottom: 0.0),
                    color: Colors.transparent,
                    child: MaterialButton(
                      minWidth: double.infinity,
                      child: Text("Create"),
                      color: Colors.amber,
                      onPressed: () {
                        createData();
                      },
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                Container(
                  padding: EdgeInsets.only(
                      top: 0.0, right: 0.0, left: 0.0, bottom: 0.0),
                  height: 40.0,
                  color: Colors.transparent,
                  child: Container(
                    padding: EdgeInsets.only(
                        top: 0.0, right: 10.0, left: 10.0, bottom: 0.0),
                    color: Colors.transparent,
                    child: MaterialButton(
                      minWidth: double.infinity,
                      child: Text("Cancel"),
                      color: Colors.blueAccent,
                      onPressed: () {
                        Navigator.pop(context, '/home');
                      },
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
              ]),
        ));
  }
}

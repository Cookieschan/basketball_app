import 'package:auto_size_text/auto_size_text.dart';
import 'package:basketball_app/model/basketball_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class MapAdminPage extends StatefulWidget {
  MapAdminPage();

  @override
  _MapAdminPageState createState() => _MapAdminPageState();
}

class _MapAdminPageState extends State<MapAdminPage> {
  var location = new Location();

  Map<String, double> userLocation;

  GoogleMapController mapController;

  List<Marker> _markers = [];

  List<Court> _courts;

  PageController _pageController;

  int prevPage;

  @override
  void initState() {
    super.initState();
    populateCourts();
    _pageController = PageController(initialPage: 1, viewportFraction: 0.8)
      ..addListener(_onScroll);
  }

  void _onScroll() {
    if (_pageController.page.toInt() != prevPage) {
      prevPage = _pageController.page.toInt();
      moveCamera();
    }
  }

  populateCourts() {
    FirebaseFirestore.instance.collection('Court').get().then((docs) {
      if (docs.docs.isNotEmpty) {
        List<Court> courtItems = [];
        List<Marker> markerItems = [];
        docs.docs.forEach((item) {
          String id = item.id;
          GeoPoint locationCoords = item.data()['locationCoords'];
          String courtName = item.data()['courtName'];
          String address = item.data()['address'];

          Court court = Court(
              id: id,
              courtName: courtName,
              address: address,
              optHours: item.data()['optHours'],
              thumbNail: item.data()['thumbNail'],
              locationCoords: locationCoords,
              surfaces: item.data()['surfaces'],
              rateIncl: item.data()['rateIncl']);
          // Add court data
          courtItems.add(court);
          // Add markers
          markerItems.add(Marker(
            markerId: MarkerId(id),
            position: LatLng(locationCoords.latitude, locationCoords.longitude),
            infoWindow: InfoWindow(title: courtName, snippet: address),
          ));
        });
        this.setState(() {
          _courts = courtItems;
          _markers = markerItems;
        });
      }
    });
  }

  _basketballCourtList(index) {
    return AnimatedBuilder(
      animation: _pageController,
      builder: (BuildContext context, Widget widget) {
        double value = 1;
        if (_pageController.position.haveDimensions) {
          value = _pageController.page - index;
          value = (1 - (value.abs() * 0.3) + 0.06).clamp(0.0, 1.0);
        }
        return Center(
          child: SizedBox(
            height: Curves.easeInOut.transform(value) * 125.0,
            width: Curves.easeInOut.transform(value) * 350.0,
            child: widget,
          ),
        );
      },
      child: InkWell(
          onTap: () {},
          child: Stack(children: [
            Center(
                child: Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: 10.0,
                      vertical: 20.0,
                    ),
                    height: 125.0,
                    width: 275.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black54,
                            offset: Offset(0.0, 4.0),
                            blurRadius: 10.0,
                          ),
                        ]),
                    child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white),
                        child: Row(children: [
                          Container(
                              height: 90.0,
                              width: 90.0,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(10.0),
                                      topLeft: Radius.circular(10.0)),
                                  image: DecorationImage(
                                      image: new NetworkImage(
                                          '${_courts[index].thumbNail}'),
                                      fit: BoxFit.cover))),
                          SizedBox(width: 7.0),
                          Container(
                            width: 170,
                            child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  AutoSizeText(
                                    '${_courts[index].courtName}',
                                    softWrap: false,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize: 12.5,
                                        fontWeight: FontWeight.bold),
                                    minFontSize: 10,
                                    maxLines: 2,
                                  ),
                                  Text(
                                    '${_courts[index].address}',
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Container(
                                    width: 170.0,
                                    child: Text(
                                      '${_courts[index].optHours}',
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize: 11.0,
                                          fontWeight: FontWeight.w300),
                                    ),
                                  )
                                ]),
                          )
                        ]))))
          ])),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Map'),
        ),
        body: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height - 50.0,
              width: MediaQuery.of(context).size.width,
              child: GoogleMap(
                initialCameraPosition: CameraPosition(
                    target: LatLng(10.720321, 122.562019), zoom: 12.0),
                markers: Set<Marker>.of(_markers),
                onMapCreated: mapCreated,
                myLocationEnabled: true,
              ),
            ),
            Positioned(
              bottom: 20.0,
              child: Container(
                height: 200.0,
                width: MediaQuery.of(context).size.width,
                child: PageView.builder(
                  controller: _pageController,
                  itemCount: _courts.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _basketballCourtList(index);
                  },
                ),
              ),
            )
          ],
        ));
  }

  void mapCreated(controller) {
    setState(() {
      mapController = controller;
    });
  }

  moveCamera() {
    mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: _markers[_pageController.page.toInt()].position,
        zoom: 20.0,
        bearing: 45.0,
        tilt: 45.0)));
  }
}

import 'package:basketball_app/widgets/brand.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TermsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _TermsPageState();
}

class _TermsPageState extends State<TermsPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.deepPurpleAccent,
        body: Stack(children: <Widget>[
          SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Brand(),
                    RichText(
                      text: TextSpan(
                        text: 'Welcome to Our App 😊 ',
                        children: <TextSpan>[
                          TextSpan(
                            text: 'Our Basketball Scheduler & Court Reservation App',
                            style: TextStyle(
                              color: Colors.black,
                              decoration: TextDecoration.underline,
                              decorationColor: Colors.red,
                              decorationStyle: TextDecorationStyle.wavy,
                            ),
                          ),
                          TextSpan(
                            text: 'builds technologies and services that enable people to Find Basketball Courts in the province of Iloilo. These Terms govern your use of our app features, apps, services, technologies and software that we offer , except where we expressly state that separate terms (and not these) apply. These Products are provided to you by Our Head Developer nga Pinaka baskog JEROME CHAVEZ III',
                          ),
                          TextSpan(
                            text: 'We dont charge you to use our App or the other products and services covered by these Terms. Instead, businesses and organisations pay us to show you ads for their products and services. By using our Products, you agree that we can show you ads that we think will be relevant to you and your interests. We use your personal data to help determine which ads to show you.',
                          ),
                      TextSpan(
                            text: 'We dont sell your personal data to advertisers, and we dont share information that directly identifies you (such as your name, email address or other contact information) with advertisers unless you give us specific permission. Instead, advertisers can tell us things such as the kind of audience that they want to see their ads, and we show those ads to people who may be interested. We provide advertisers with reports about the performance of their ads that help them understand how people are interacting with their content. See Section 2 below to learn more.',
                          ),
                          TextSpan(
                            text: 'Our Data Policy explains how we collect and use your personal data to determine some of the ads that you see and provide all of the other services described below. You can also go to your settings at any time to review the privacy choices you have about how we use your data.',
                          ),
                        ],
                      ),
                    )
                  ]))
        ]));
  }
}

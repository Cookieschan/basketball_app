import 'package:basketball_app/firebase/reserveservice.dart';
import 'package:basketball_app/model/reservefunction.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/rendering.dart';
import 'package:basketball_app/firebase/database.dart';

class ReserveFormView extends StatefulWidget {
  ReserveFormView({Key key, this.database}) : super(key: key);
  Database database;
  @override
  _ReserveFormViewState createState() => _ReserveFormViewState();
}

class _ReserveFormViewState extends State<ReserveFormView> {
  List<Reserve> items;
  ReserveService reserveService = new ReserveService();
  StreamSubscription<QuerySnapshot> toReserve;

  @override
  void initState() {
    super.initState();

    items = [];

    toReserve?.cancel();
    toReserve =
        reserveService.getReserveList().listen((QuerySnapshot snapshot) {
      final List<Reserve> reserve = snapshot.docs
          .map((documentSnapshot) => Reserve.fromMap((documentSnapshot.data())))
          .toList();

      setState(() {
        this.items = reserve.cast<Reserve>();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Reserve Request'),
        ),
        resizeToAvoidBottomInset: false,
        body: Column(
          children: <Widget>[
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 615.0,
                child: ListView.builder(
                    itemCount: items.length,
                    itemBuilder: (context, index) {
                      return Stack(children: <Widget>[
                        // The containers in the background
                        Column(children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(left: 8.0, right: 8.0),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: 110.0,
                              child: Padding(
                                padding: EdgeInsets.only(top: 8.0, bottom: 2.0),
                                child: Material(
                                  elevation: 2.0,
                                  shadowColor: Color(0x802196F3),
                                  child: Center(
                                    child: Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Text(
                                                  'Name: ${items[index].reserveName}',
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 20.0,
                                                  ),
                                                ),
                                              ]),
                                          SizedBox(
                                            height: 5.0,
                                          ),
                                          Text(
                                            'Contact: +63${items[index].reserveNumber}',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 10.0,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 5.0,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                '${items[index].reserveTimeStart} - ',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 10.0),
                                              ),
                                              SizedBox(
                                                width: 5.0,
                                              ),
                                              Text(
                                                '${items[index].reserveTimeEnd},',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 10.0),
                                              ),
                                              SizedBox(
                                                width: 5.0,
                                              ),
                                              Text(
                                                '${items[index].reserveDate}',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 10.0),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 5.0,
                                          ),
                                          Text(
                                            '${items[index].reserveCourtName}',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 15.0),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ]),
                      ]);
                    }),
              ),
            ),
          ],
        ));
  }
}

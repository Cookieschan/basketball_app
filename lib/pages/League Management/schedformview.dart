import 'package:flutter/material.dart';
import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:basketball_app/firebase/schedservice.dart';
import 'package:basketball_app/model/schedulefunction.dart';
import 'package:flutter/rendering.dart';
/*

class SchedFormView extends StatefulWidget {
  SchedFormView({Key key}) : super(key: key);
  @override
  _SchedFormViewState createState() => _SchedFormViewState();
}

class _SchedFormViewState extends State<SchedFormView> {
  List<Schedule> items;
  SchedService schedServ = new SchedService();
  StreamSubscription<QuerySnapshot> toScheds;
  final ds = Firestore.instance;
  String teamOne, teamTwo, teamTime, teamDate, teamAddress;

  getteamOne(teamOne) {
    this.teamOne = teamOne;
  }

  getteamTwo(teamTwo) {
    this.teamTwo = teamTwo;
  }

  getteamTime(teamTime) {
    this.teamTime = teamTime;
  }

  getteamDate(teamDate) {
    this.teamDate = teamDate;
  }

  getteamAddress(teamAddress) {
    this.teamAddress = teamAddress;
  }


  @override
  void initState() {
    super.initState();

    items = new List();

    toScheds?.cancel();
    toScheds = schedServ.getScheduleList().listen((QuerySnapshot snapshot) {
      final List<Schedule> scheds = snapshot.documents
          .map((documentSnapshot) => Schedule.fromMap(documentSnapshot.data))
          .toList();

      setState(() {
        this.items = scheds;
      });
    });
  }

 Card buildItem(DocumentSnapshot doc) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              '${doc.data['teamOne']}',
              style: TextStyle(fontSize: 25 , fontWeight: FontWeight.bold),
            ),
            Text(
              'VS',
              style: TextStyle(fontSize: 10 , fontWeight: FontWeight.bold),
            ),

            Text(
              '${doc.data['teamTwo']}',
              style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold),
            ),
            Text(
              '${doc.data['teamTime']}',
              style: TextStyle(fontSize: 20),
            ),
            Text(
              '${doc.data['teamDate']}',
              style: TextStyle(fontSize: 20),
            ),
            Text(
              '${doc.data['teamAddress']}',
              style: TextStyle(fontSize: 23),
            ),
                                                                  
                                                                  SizedBox(
                                                                      height:
                                                                          12),
                                                                  Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .end,
                                                                      children: <
                                                                          Widget>[
                                                                        FlatButton(
                                                                          onPressed:
                                                                              () {
                                                                            showDialog<void>(
                                                                                context: context,
                                                                                builder: (BuildContext context) {
                                                                                  return AlertDialog(
                                                                                      backgroundColor: Colors.deepPurpleAccent,
                                                                                      content: SingleChildScrollView(
                                                                                          scrollDirection: Axis.vertical,
                                                                                          child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                                                                                            Container(
                                                                                                padding: EdgeInsets.only(top: 10.0, left: 20.0, right: 20.0),
                                                                                                child: Form(
                                                                                                    child: Column(
                                                                                                  children: <Widget>[
                                                                                                    TextFormField(
                                                                                                      style: TextStyle(color: Colors.white),
                                                                                                      decoration: InputDecoration(labelText: 'Input First Team', labelStyle: TextStyle(fontFamily: 'Montserrat', color: Colors.white), focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white))),
                                                                                                      validator: (value) => value.isEmpty ? 'Title can\'t be empty' : null,
                                                                                                      onChanged: (String teamOne) {
                                                                                                        getteamOne(teamOne);
                                                                                                      },
                                                                                                    ),
                                                                                                    TextFormField(
                                                                                                      style: TextStyle(color: Colors.white),
                                                                                                      decoration: InputDecoration(labelText: 'Input Second Team', labelStyle: TextStyle(fontFamily: 'Montserrat', color: Colors.white), focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white))),
                                                                                                      validator: (value) => value.isEmpty ? 'Title can\'t be empty' : null,
                                                                                                      onChanged: (String teamTwo) {
                                                                                                        getteamTwo(teamTwo);
                                                                                                      },
                                                                                                    ),
                                                                                                    TextFormField(
                                                                                                      style: TextStyle(color: Colors.white),
                                                                                                      decoration: InputDecoration(labelText: 'Input Time', labelStyle: TextStyle(fontFamily: 'Montserrat', color: Colors.white), focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white))),
                                                                                                      validator: (value) => value.isEmpty ? 'Title can\'t be empty' : null,
                                                                                                      onChanged: (String teamTime) {
                                                                                                        getteamTime(teamTime);
                                                                                                      },
                                                                                                    ),
                                                                                                    TextFormField(
                                                                                                      style: TextStyle(color: Colors.white),
                                                                                                      decoration: InputDecoration(labelText: 'Input Date', labelStyle: TextStyle(fontFamily: 'Montserrat', color: Colors.white), focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white))),
                                                                                                      validator: (value) => value.isEmpty ? 'Title can\'t be empty' : null,
                                                                                                      onChanged: (String teamDate) {
                                                                                                        getteamDate(teamDate);
                                                                                                      },
                                                                                                    ),
                                                                                                    TextFormField(
                                                                                                      style: TextStyle(color: Colors.white),
                                                                                                      decoration: InputDecoration(labelText: 'Input Address', labelStyle: TextStyle(fontFamily: 'Montserrat', color: Colors.white), focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white))),
                                                                                                      validator: (value) => value.isEmpty ? 'Title can\'t be empty' : null,
                                                                                                      onChanged: (String teamAddress) {
                                                                                                        getteamAddress(teamAddress);
                                                                                                      },
                                                                                                    ),

                                                                                                    FlatButton(
                                                                                                      child: Text('Ok'),
                                                                                                      onPressed: () {
                                                                                                        updateData(doc.documentID,teamOne,teamTwo,teamTime,teamDate,teamAddress);
                                                                                                        Navigator.pop(context);
                                                                                                      },
                                                                                                    ),
                                                                                                  ],
                                                                                                )))
                                                                                          ])));
                                                                                });
                                                                          },
                                                                          child: Text(
                                                                              'Update event',
                                                                              style: TextStyle(color: Colors.white)),
                                                                          color:
                                                                              Colors.green,
                                                                        ),
                                                                        SizedBox(
                                                                            width:
                                                                                8),
                                                                        FlatButton(
                                                                          child: Text(
                                                                              'Delete',
                                                                              style: TextStyle(color: Colors.white)),
                                                                          color:
                                                                              Colors.red,
                                                                          onPressed: () {
                                                                            deleteData(doc);

                                                                          }
                                                                            
                                                                        ),
                                                                      ])
                                                                ])));
                                  
                                
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Schedules'),
      ),
      body: ListView(
        padding: EdgeInsets.all(8),
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[],
          ),
          StreamBuilder<QuerySnapshot>(
            stream: ds.collection('schedules').snapshots(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Column(
                    children: snapshot.data.documents
                        .map((documentSnapshot) => buildItem(documentSnapshot))
                        .toList());
              } else {
                return SizedBox();
              }
            },
          )
        ],
      ),
    );
  }

  void updateData(String documentID, String teamOne, String teamTwo, String teamTime, String teamDate, String teamAddress) async {
    print("Updating data...");
    return await ds.collection('schedules').document(documentID).updateData({
      "teamOne": this.teamOne,
      "teamTwo": this.teamTwo,
      "teamTime": this.teamTime,
      "teamDate": this.teamDate,
      "teamAddress": this.teamAddress
    });
  }

  void deleteData(DocumentSnapshot doc) async {
    try {
      await ds.collection('schedules').document(doc.documentID).delete();
      setState(() => this.items == null);
      
      
    } catch (e) {
      
    }
  }
}
*/
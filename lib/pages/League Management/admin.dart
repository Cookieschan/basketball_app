import 'package:flutter/material.dart';


class DashboardButton extends StatelessWidget {
  const DashboardButton({
    Key key,
    @required this.icon,
    @required this.text,
    this.onTap,
  }) : super(key: key);

  final IconData icon;
  final String text;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: onTap,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            FractionallySizedBox(
              widthFactor: 0.2,
              child: FittedBox(
                child: Icon(icon),
              ),
            ),
            
            Text(
              text,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30.0,
                              
              ),
              textScaleFactor: 0.8,
            ),
          ]
        ),
      ),
    );
  }
}

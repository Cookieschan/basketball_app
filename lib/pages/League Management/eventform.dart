import 'package:basketball_app/widgets/form_title.dart';
import 'package:flutter/material.dart';
import 'package:basketball_app/widgets/brand.dart';
import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class EventFormPage extends StatefulWidget {
  EventFormPage();
  @override
  State<StatefulWidget> createState() => new _EventFormPageState();
}

class _EventFormPageState extends State<EventFormPage> {
  final _formKey = GlobalKey<FormState>();
  String eventTitle, eventTime, eventDate, eventDescription;
  String _date = "Not set";
  String _time = "Not set";

  geteventTitle(eventTitle) {
    this.eventTitle = eventTitle;
  }

  geteventTime(eventTime) {
    this.eventTime = eventTime;
  }

  geteventDate(eventDate) {
    this.eventDate = eventDate;
  }

  geteventDescription(eventDescription) {
    this.eventDescription = eventDescription;
  }

  createData() {
    DocumentReference ds =
        FirebaseFirestore.instance.collection('events').doc();
    Map<String, dynamic> event = {
      "eventTitle": eventTitle,
      "eventTime": eventTime,
      "eventDate": eventDate,
      "eventDescription": eventDescription,
    };
    if (_formKey.currentState.validate())
      ds.set(event).whenComplete(() {
        print("New Event is added");
      });
    if (_formKey.currentState.validate()) {
      return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Success'),
            content: const Text('new event is added'),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.deepPurpleAccent,
        body: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Brand(),
                  Container(
                    padding:
                        EdgeInsets.only(top: 10.0, left: 20.0, right: 20.0),
                    child: Form(
                      key: _formKey,
                      child: Column(children: <Widget>[
                        FormTitle(text: "Create Event"),
                        TextFormField(
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              labelText: 'Set Title',
                              labelStyle: TextStyle(
                                  fontFamily: 'Montserrat',
                                  color: Colors.white),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                          validator: (value) =>
                              value.isEmpty ? 'Title can\'t be empty' : null,
                          onChanged: (String title) {
                            geteventTitle(title);
                          },
                        ),
                        RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0)),
                          elevation: 4.0,
                          onPressed: () {
                            DatePicker.showDatePicker(context,
                                theme: DatePickerTheme(
                                  containerHeight: 210.0,
                                ),
                                showTitleActions: true,
                                minTime: DateTime(2000, 1, 1),
                                maxTime: DateTime(2022, 12, 31),
                                onConfirm: (date) {
                              print('confirm $date');
                              _date =
                                  '${date.year} - ${date.month} - ${date.day}';
                              setState(() {});
                              geteventDate(" $_date");
                            },
                                currentTime: DateTime.now(),
                                locale: LocaleType.en);
                          },
                          child: Container(
                            alignment: Alignment.center,
                            height: 20.0,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Container(
                                      child: Row(
                                        children: <Widget>[
                                          Icon(
                                            Icons.date_range,
                                            size: 18.0,
                                            color: Colors.black87,
                                          ),
                                          Text(
                                            " $_date",
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18.0),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                Text(
                                  "  Change",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.0),
                                ),
                              ],
                            ),
                          ),
                          color: Colors.white,
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0)),
                          elevation: 4.0,
                          onPressed: () {
                            DatePicker.showTimePicker(context,
                                theme: DatePickerTheme(
                                  containerHeight: 210.0,
                                ),
                                showTitleActions: true, onConfirm: (time) {
                              print('confirm $time');
                              _time =
                                  '${time.hour} - ${time.minute} - ${time.second}';
                              setState(() {});
                              geteventTime(" $_time");
                            },
                                currentTime: DateTime.now(),
                                locale: LocaleType.en);
                          },
                          child: Container(
                            alignment: Alignment.center,
                            height: 20.0,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Container(
                                      child: Row(
                                        children: <Widget>[
                                          Icon(
                                            Icons.access_time,
                                            size: 18.0,
                                            color: Colors.black87,
                                          ),
                                          Text(
                                            " $_time",
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18.0),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                Text(
                                  "  Change",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.0),
                                ),
                              ],
                            ),
                          ),
                          color: Colors.white,
                        ),
                        SizedBox(height: 20.0),
                        TextFormField(
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              labelText: 'Set Description',
                              labelStyle: TextStyle(
                                  fontFamily: 'Montserrat',
                                  color: Colors.white),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                          validator: (value) => value.isEmpty
                              ? 'Description can\'t be empty'
                              : null,
                          onChanged: (String description) {
                            geteventDescription(description);
                          },
                        ),
                      ]),
                    ),
                  ),
                  SizedBox(height: 40.0),
                  Container(
                    padding: EdgeInsets.only(
                        top: 0.0, right: 0.0, left: 0.0, bottom: 0.0),
                    height: 40.0,
                    color: Colors.transparent,
                    child: Container(
                      padding: EdgeInsets.only(
                          top: 0.0, right: 10.0, left: 10.0, bottom: 0.0),
                      color: Colors.transparent,
                      child: MaterialButton(
                        minWidth: double.infinity,
                        child: Text("Create"),
                        color: Colors.amber,
                        onPressed: () {
                          createData();
                        },
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Container(
                    padding: EdgeInsets.only(
                        top: 0.0, right: 0.0, left: 0.0, bottom: 0.0),
                    height: 40.0,
                    color: Colors.transparent,
                    child: Container(
                      padding: EdgeInsets.only(
                          top: 0.0, right: 10.0, left: 10.0, bottom: 0.0),
                      color: Colors.transparent,
                      child: MaterialButton(
                        minWidth: double.infinity,
                        child: Text("Cancel"),
                        color: Colors.blueAccent,
                        onPressed: () {
                          Navigator.pop(context, '/home');
                        },
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0),
                ])));
  }
}

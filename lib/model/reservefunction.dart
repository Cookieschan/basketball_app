class Reserve{
  String _reserveName;
  String _reserveNumber;
  String _reserveTimeStart;
  String _reserveTimeEnd;
  String _reserveDate;
  String _reserveCourtName;
  

  Reserve(this._reserveName, this._reserveNumber, this._reserveTimeStart, this._reserveTimeEnd, this._reserveDate,this._reserveCourtName);

  Reserve.map(dynamic obj){
    this._reserveName = obj['Name'];
    this._reserveNumber = obj['Number'];
    this._reserveTimeStart = obj['TimeStart'];
    this._reserveTimeEnd = obj['TimeEnd'];
    this._reserveDate = obj['Date'];
    this._reserveCourtName = obj['CourtName'];
  }

  String get reserveName => _reserveName;
  String get reserveNumber => _reserveNumber;
  String get reserveTimeStart => _reserveTimeStart;
  String get reserveTimeEnd => _reserveTimeEnd;
  String get reserveDate => _reserveDate;
  String get reserveCourtName => _reserveCourtName;


  Map<String,dynamic> toMap(){
    var map=new Map<String,dynamic>();
    map['Name']=_reserveName;
    map['Number']=_reserveNumber;
    map['TimeStart'] = _reserveTimeStart;
    map['TimeEnd'] = _reserveTimeEnd;
    map['Date'] = _reserveDate;
    map['CourtName'] = _reserveCourtName;

    return map;
  }

  Reserve.fromMap(Map<String,dynamic> map){
    this._reserveName = map['Name'];
    this._reserveNumber = map['Number'];  
    this._reserveTimeStart = map['TimeStart'];
    this._reserveTimeEnd = map['TimeEnd'];
    this._reserveDate = map['Date'];
    this._reserveCourtName = map['CourtName'];
  }
}
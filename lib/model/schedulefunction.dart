class Event{
  String key;
  String _teamOne;
  String _teamTwo;
  String _teamDate;
  String _teamTime;

  Event(this.key,this._teamOne,this._teamTwo,this._teamDate,this._teamTime);

  Event.map(dynamic obj){
    this._teamOne = obj['teamOne'];
    this._teamTwo = obj['teamTwo'];
    this._teamDate = obj['teamDate'];
    this._teamTime = obj['teamTime'];
  }

  String get teamOne => _teamOne;
  String get teamTwo => _teamTwo;
  String get teamDate => _teamDate;
  String get teamTime => _teamTime;

  Map<String,dynamic> toMap(){
    var map=new Map<String,dynamic>();
    map['teamOne']= _teamOne;
     map['teamTwo'] = _teamTwo;
    map['teamDate'] = _teamDate;
    map['teamTime'] = _teamTime;
    return map;
  }

  Event.fromMap(Map<String,dynamic> map){
    this._teamOne= map['teamOne'];
    this._teamTwo = map['teamTwo'];
    this._teamDate = map['teamDate'];
    this._teamTime = map['teamTime'];
  }
}

class Court{
  String key;
  String _courtName;
  String _address;
  

  Court(this.key,this._courtName,this._address,);

  Court.map(dynamic obj){
    this._courtName = obj['courtName'];
    this._address = obj['address'];
  }

  String get courtName => _courtName;
  String get address => _address;

  Map<String,dynamic> toMap(){
    var map=new Map<String,dynamic>();
    map['courtName']= _courtName;
     map['address'] = _address;
    return map;
  }

  Court.fromMap(Map<String,dynamic> map){
    this._courtName= map['courtName'];
    this._address = map['address'];
  }
}
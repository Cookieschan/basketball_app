import 'package:cloud_firestore/cloud_firestore.dart';
/*
class Court {
  String courtName;
  String address;
  String optHours;
  String thumbNail;
  LatLng locationCoords;
  String rateIncl;
  String surfaces;

  Court(
      {this.courtName,
      this.address,
      this.optHours,
      this.thumbNail,
      this.locationCoords,
      this.rateIncl,
      this.surfaces});
}

final List<Court> basketballCourts = [
  Court(
      courtName: 'Aganan Basketball Court',
      address: 'Brgy. Aganan, Pavia, Iloilo',
      optHours: '6:00 AM - 9:00 PM',
      locationCoords: LatLng(10.770555, 122.535945),
      thumbNail:
          'https://geo2.ggpht.com/cbk?panoid=DnPfwpMuR9bP_97JwFsPwQ&output=thumbnail&cb_client=search.gws-prod.gps&thumb=2&w=408&h=306&yaw=312.3613&pitch=0&thumbfov=100',
      rateIncl: '₱ 350/hr',
      surfaces: 'Concrete'),
  Court(
      courtName: 'Brgy Buntatala Basketball Court',
      address: 'Brgy. Buntatala Leganes, Iloilo',
      optHours: '6:00 AM - 9:00 PM',
      locationCoords: LatLng(10.776539, 122.584544),
      thumbNail:
          'https://geo0.ggpht.com/cbk?panoid=drQaT6_zh5NV5F8AL0P9CQ&output=thumbnail&cb_client=search.gws-prod.gps&thumb=2&w=408&h=306&yaw=267.48706&pitch=0&thumbfov=100',
      rateIncl: '₱ 350/hr',
      surfaces: 'Concrete'),
];
*/

class Court {
  final String id;
  final String courtName;
  final String address;
  final String optHours;
  final String thumbNail;
  GeoPoint locationCoords;
  final String rateIncl;
  final String surfaces;

  Court({
    this.id,
    this.courtName,
    this.address,
    this.optHours,
    this.thumbNail,
    this.locationCoords,
    this.rateIncl,
    this.surfaces,
  });

  factory Court.fromJson(Map<String, dynamic> json) {
    return Court(
      id: json['id'],
      courtName: json['courtName'],
      address: json['address'],
      optHours: json['optHours'],
      thumbNail: json['thumbNail'],
      locationCoords: json['locationCoords'],
      rateIncl: json['rateIncl'],
      surfaces: json['surfaces'],
    );
  }
}

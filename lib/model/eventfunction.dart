class Sched{
  String _eventTitle;
  String _eventTime;
  String _eventDate;
  String _eventDescription;

  Sched(this._eventTitle,this._eventTime,this._eventDate,this._eventDescription);

  Sched.map(dynamic obj){
    this._eventTitle = obj['eventTitle'];
    this._eventTime = obj['eventTime'];
    this._eventDate = obj['eventDate'];
    this._eventDescription = obj['eventDescription'];
  }

  String get eventTitle => _eventTitle;
  String get eventTime => _eventTime;
  String get eventDate => _eventDate;
  String get eventDescription => _eventDescription;

  Map<String,dynamic> toMap(){
    var map=new Map<String,dynamic>();
    map['eventTitle']=_eventTitle;
     map['eventTime'] = _eventTime;
    map['eventDate'] = _eventDate;
    map['eventDescription'] = _eventDescription;
    return map;
  }

  Sched.fromMap(Map<String,dynamic> map){
    this._eventTitle= map['eventTitle'];
    this._eventTime = map['eventTime'];
    this._eventDate = map['eventDate'];
    this._eventDescription = map['eventDescription'];
  }
}
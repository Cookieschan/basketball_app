import 'package:basketball_app/firebase/auth.dart';
import 'package:basketball_app/firebase/database.dart';
import 'package:basketball_app/pages/League Management/eventformview.dart';
import 'package:basketball_app/pages/League Management/mapadmin.dart';
import 'package:basketball_app/pages/League Management/reserveformview.dart';
import 'package:basketball_app/pages/League%20Management/eventform.dart';
import 'package:basketball_app/pages/League%20Management/scheduleform.dart';
import 'package:basketball_app/pages/League%20Management/termscon.dart';
import 'package:basketball_app/pages/dashboard/mapreservationform.dart';
import 'package:basketball_app/pages/home.dart';
import 'package:basketball_app/pages/login.dart';
import 'package:basketball_app/pages/root.dart';
import 'package:basketball_app/pages/settings.dart';
import 'package:basketball_app/pages/signup.dart';
import 'package:basketball_app/pages/theme.dart';
import 'package:basketball_app/pages/themenotifier.dart';
import 'package:basketball_app/pages/welcome.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();

  print("Handling a background message: ${message.messageId}");
}

const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'high_importance_channel', // id
  'High Importance Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.high,
);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  FirebaseMessaging.instance.subscribeToTopic('events');

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  runApp(
    ChangeNotifierProvider<ThemeNotifier>(
      create: (_) => ThemeNotifier(lightTheme),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  FirebaseAnalytics analytics = FirebaseAnalytics();

  @override
  Widget build(BuildContext context) {
    final themeNotifier = Provider.of<ThemeNotifier>(context);
    return MaterialApp(
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
      title: 'Basketball App',
      debugShowCheckedModeBanner: false,
      theme: themeNotifier.getTheme(),
      routes: <String, WidgetBuilder>{
        '/signup': (BuildContext context) => new SignUpPage(auth: new Auth()),
        '/login': (BuildContext context) => new LoginPage(auth: new Auth()),
        '/home': (BuildContext context) =>
            new Home(auth: new Auth(), database: new Database()),
        '/eventform': (BuildContext context) => new EventFormPage(),
        '/mapadmin': (BuildContext context) => new MapAdminPage(),
        '/eventformview': (BuildContext context) => new EventFormView(),
        '/scheduleform': (BuildContext context) => new SchedFormPage(),
        //'/schedformview': (BuildContext context) => new SchedFormView(),
        '/reserveformview': (BuildContext context) => new ReserveFormView(),
        '/mapreservationform': (BuildContext context) =>
            new MapreservationFormPage(),
        '/welcome': (BuildContext context) => new WelcomePage(),
        '/termscon': (BuildContext context) => new TermsPage(),
        '/settings': (BuildContext context) => new SettingsPage(),
      },
      home: new RootPage(auth: new Auth(), database: new Database()),
    );
  }
}

import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as serviceAccount from './serviceaccountKey.json';
const cert = serviceAccount as any

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://basketball-app-a2481.firebaseio.com"
});

// const db = admin.firestore();
const fcm = admin.messaging();

// console.log({db,fcm})
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
export const createDefaultRole = functions.auth.user().onCreate((user) => {
    admin.firestore().collection("users").doc(user.uid).set({ role: "member" }).catch((error) => {
        console.log(error);
    })
});

export const sendToTopic = functions.firestore
  .document('events/{eventId}')
  .onCreate(snapshot => {
    const event:any = snapshot.data();
    console.log('connected');

    const payload: admin.messaging.MessagingPayload = {
      notification: {
        title: `${event.eventTitle}`,
        body: `${event.eventDescription}`,
        icon: 'https://geo2.ggpht.com/cbk?panoid=DnPfwpMuR9bP_97JwFsPwQ&output=thumbnail&cb_client=search.gws-prod.gps&thumb=2&w=408&h=306&yaw=312.3613&pitch=0&thumbfov=100',
        click_action: 'FLUTTER_NOTIFICATION_CLICK' // required only for onResume or onLaunch callbacks
      }
    };

    return fcm.sendToTopic('events', payload).catch(err=>console.log(err))
  });

  export const testFCM = functions.app.admin.messaging().sendToTopic('events', {
    notification: {
      title: 'hello',
      body: 'sdfsadfadsfdas',
      icon: '',
      click_action: 'FLUTTER_NOTIFICATION_CLICK' // required only for onResume or onLaunch callbacks
    }
  }).catch(err=>console.log(err))

  export const updateSched = functions.https.onCall((data:any, _context:any) => {
    const users = admin.firestore().collection('schedules');
    users.doc(data["doc_id"]).set({
        teamOne: data["teamOne"],
        teamTwo: data["teamTwo"],
        teamTime: data["teamTime"],
        teamDate: data["teamDate"],
        teamAddress: data["teamAddress"],
    }).then(res=>console.log(res)).catch(err=>console.log(err))
});

export const addSched = functions.https.onCall((data:any, _context:any) => {
  const users = admin.firestore().collection('schedules');
  return users.add({
      teamOne: data["teamOne"],
      teamTwo: data["teamTwo"],
      teamTime: data["teamTime"],
      teamDate: data["teamDate"],
      teamAddress: data["teamAddress"],
    }).then(res=>console.log(res)).catch(err=>console.log(err))
});